<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\CitasAdmnistradorController;
use App\Http\Controllers\CapitulosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Página principal
Route::get('/', function () {
    return view('index');
});
//Página de información
Route::get('/informacion', function () {
    return view('informacion');
});
//Página que lleva a la muestra de la galería
Route::get('/galeria', function () {
    return view('galeria');
});
//Pagina que lleva a la vista de preguntas
Route::get('/preguntas', function () {
    return view('preguntas');
});

//Página get y post de la vista de contacto
Route::get('/contacto','App\Http\Controllers\ContactoController@contacto');
Route::post('/contacto','App\Http\Controllers\ContactoController@contacto');

///Paguna de Listado de Capitulos
Route::get('/AdminCapitulos', function () {
    return view('AdminCapitulos');
});
////Página de Agendar cita (Create)
Route::get('/AgregarCap', function () {
    return view('AgregarCap');
});
//Método POST que hace el guardado
Route::post('/savee','App\Http\Controllers\CapitulosController@savee')->name('savee');
//Página de citas del admin (El listado)
Route::get('/AdminCapitulos','App\Http\Controllers\CapitulosController@list');
//Página del delete los capitulos
Route::delete('/borrar/{id}','App\Http\Controllers\CapitulosController@borrar')->name('borrar');
//Listado
Route::get('/AdminCapitulos','App\Http\Controllers\CapitulosController@list');
//Formulario del actualizar los capitulos 
Route::get('/EditarCap/{id}','App\Http\Controllers\CapitulosController@EditarCap')->name('EditarCap');
//Acción de edit para modificar registros
Route::patch('/edit/{id}','App\Http\Controllers\CapitulosController@edit')->name('edit');

////Página de Agendar cita (Create)
Route::get('/AgendarCitaAdmin', function () {
    return view('AdminDashboardAgendarCita');
});
//Método POST que hace el guardado
Route::post('/save','App\Http\Controllers\CitasAdministradorController@save')->name('save');
//Vista del Dashboard del administrador
Route::get('/AdminDashboard', function () {
    return view('AdminDashboard');
});
//Ruta que muestra los capitulos al estudiantes 
Route::get('/capitulos', function () {
    return view('capitulos');
});
//Ruta para ir a las Citas de los Estudiantes
Route::get('/CitasEstudiante', function () {
    return view('CitasEstudiante');
});

//Rutas de que obtiene los datos de los datos en la pagina de contacto
Route::get('/contacto','App\Http\Controllers\ContactoController@contacto');
Route::post('/contacto','App\Http\Controllers\ContactoController@contacto');

//Página de citas del admin (El listado)
Route::get('/CitasAdmin','App\Http\Controllers\CitasAdministradorController@list');
//Página del delete de citas del admin
Route::delete('/delete/{CitaId}','App\Http\Controllers\CitasAdministradorController@delete')->name('delete');
//Listado
Route::get('/CitasAdmin','App\Http\Controllers\CitasAdministradorController@list');
//Formulario del actualizar
Route::get('/EditarCita/{CitaId}','App\Http\Controllers\CitasAdministradorController@EditarCita')->name('EditarCita');

//Acción de edit para modificar registros
Route::patch('/edit/{CitaId}','App\Http\Controllers\CitasAdministradorController@edit')->name('edit');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Ruta que permite el registro 
Route::get('/Registro', function () {
    return view('auth.register');
});
//Ruta que lleva al login 
Route::get('/Login', function () {
    return view('auth.login');
});
//Ruta de que obtiene los resultados de los examenes
Route::get('/examen','App\Http\Controllers\ExamenController@exam');
//Método POST que hace el guardado
Route::post('/guardarresultados','App\Http\Controllers\ExamenController@guardarresultados')->name('guardarresultados');
//Página de citas del admin (El listado)
///Pagina de Listado de las preguntas
Route::get('/AdminPreguntas', function () {
    return view('AdminCapitulos');
});
////Página para agregar preguntas (Create)
Route::get('/AgregarPregunta', function () {
    return view('AgregarPregunta');
});

Route::get('/Resultados', function () {
    return view('retro');
});
//Método POST que hace el guardado
Route::post('/saveee','App\Http\Controllers\PreguntasController@saveee')->name('saveee');
//Devuelve el listado de las preguntas 
Route::get('/AdminPreguntas','App\Http\Controllers\PreguntasController@list');
//Página de borrado de las preguntas
//Route::delete('/delete/{id}','App\Http\Controllers\PreguntasController@delete')->name('delete');
//Listado
Route::get('/AdminPreguntas','App\Http\Controllers\PreguntasController@list');
//Formulario del actualizar
Route::get('/EditarPreg/{id}','App\Http\Controllers\PreguntasController@EditarPreg')->name('EditarPreg');
//Acción de edit para modificar registros
Route::patch('/edit/{id}','App\Http\Controllers\PreguntasController@edit')->name('edit');


