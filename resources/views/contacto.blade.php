<?php

        $msg = '';
        if(isset($_POST['submit'])){

          require '../vendor/autoload.php';
          require '../public/info.php';
        // Create the Transport
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 587,'tls'))
          ->setUsername(EMAIL)
          ->setPassword(PASS)
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message('Mensaje nuevo - Escuela de Manejo CR'))
          ->setFrom([EMAIL => 'EscuelaManejoCr'])
          ->setTo(['descuelamanejo2021@gmail.com'])
          ->setBody('Nombre: '.$_POST['name'].'<br>
                     Email: '.$_POST['email'].'<br> 
                     Telefóno: '.$_POST['phone'].'<br>
                     Mensaje: '.$_POST['msg'],
                     'text/html')
          ;

        // Send the message
        $result = $mailer->send($message);

        if (!$result) {
          $msg = '<div class="alert alert-danger text-center">
                      ¡Ocurrio un error!
                  </div>';
    }
    else{
      $msg = '<div class="alert alert-success text-center">
                  ¡Mensaje Enviado Correctamente!
              </div>';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Escuela de Manejo CR - Contacto</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{asset('/img/favicon.png') }}" rel="icon">
  <link href="{{asset('/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="/" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="/">Inicio</a></li>
          <li class="drop-down"><a href="/capitulos">Lector del manual</a>
            <ul>
              <li><a href="#">Capítulo 1</a></li>
              <li><a href="#">Capítulo 2</a></li>
              <li><a href="#">Capítulo 3</a></li>
              <li><a href="#">Capítulo 4</a></li>
              <li><a href="#">Capítulo 5</a></li>
              <li><a href="#">Capítulo 6</a></li>
              <li><a href="#">Capítulo 7</a></li>
              <li><a href="#">Capítulo 8</a></li>
              <li><a href="#">Capítulo 9</a></li>
              <li><a href="#">Capítulo 10</a></li>
            </ul>
          </li>
          <li><a href="/galeria">Galería</a></li>
          <li><a href="/informacion">Información</a></li>
          
          <li><a href="/contacto">Contacto</a></li>
      <li><a href="/login">Iniciar Sesión</a></li> 
          <li><a href="/register">Registrarse</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->


  <main id="main">

<div>
    
   <div class="conteiner" style="padding-top:90px; paddind-bottom:70px">
     <div class="row">
       <div class="col-md-6 offset-md-3">
         <div class="card">
          <div class="card-header">
            Contáctenos
          </div>
          <div class="card-body">
            <div><?= $msg; ?> </div>
            <form method="post" action="" enctype="multipart/form-data">
              {{method_field('post')}}
              @csrf
              <div class="form-group">
                <label for="name">Nombre Completo</label>
                <input type="text" name="name" placeholder="Nombre y apellidos" minlength="3" class="form-control" required />
              </div>
              <div class="form-group">
                <label for="email">Correo electrónico</label>
                <input type="email" name="email"  placeholder="tucorreo@email.com" class="form-control" required />
              </div>
              <div class="form-group">
                <label for="phone">Teléfono</label>
                <input type="text" name="phone" placeholder="Ejm: 89895432" minlength="8" maxlength="8" class="form-control" required />
              </div>
              <div class="form-group">
                <label for="msg"> Mensaje</label>
                <textarea name="msg" class="form-control" placeholder="Mensaje" required></textarea> 
              </div>
              <input type="submit" name="submit" class="btn btn-primary float-right" value="Enviar"/>
            </form>
          </div>
         </div>
       </div>
     </div>
   </div>

  </div>



  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container" style="padding-top: 20px">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Escuela de Manejo CR</h3>
            <p>La escuela de manejo CR esta abierta para todos aquellas personas que quieran aprender a manejar.</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Enlaces</h4>
            <ul>
              <li><a href="/">Inicio</a></li>
              <li><a href="/informacion">Informacion</a></li>
              <li><a href="/galeria">Galeria</a></li>
              <li><a href="/informacion">Información</a></li>
            
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contactanos</h4>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br>
              <strong>Telefóno:</strong> +1 5589 55488 55<br>
              <strong>Correo electrónico:</strong> info@example.com<br>
            </p>

            <div class="social-links">

              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; <strong>Escuela Manejo CR</strong>.
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->

      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script language="JavaScript">

    window.onbeforeunload = preguntarAntesDeSalir;
    
    function preguntarAntesDeSalir(){
    return "¿Seguro que quieres salir del formulario?";
    }
    
    </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
