<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <title>Escuela de Manejo CR</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="css/AdminDashboard.css">
    <link rel="stylesheet" href="css/IndexCitas.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <input type="checkbox" id="sidebar-toggle">
    <div class="sidebar">
        <div class="sidebar-header">
            <h3 class="brand">
                <span>Administrador</span>
            </h3>
            <label for="sidebar-toggle" class="ti-menu-alt"></label>
        </div>

        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="/">
                        <span class="ti-home"></span>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-user"></span>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-agenda"></span>
                        <span>Tareas</span>
                    </a>
                </li>
                <li>
                    <a href="AdminDashboardCitas.html">
                        <span class="ti-calendar"></span>
                        <span>Programar clases</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-book"></span>
                        <span>Capitulos</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 mt-5">          
                    <h2 class="text-center mb-5">Listado de Capitulos<h2>
                        <a class="btn btn-success mb-2" href="{{url('/AgregarCap')}}">Agregar Capitulo</a>
                        <table class="table table-bordered table-strpied text-center">
                            <thead>
                                <tr>
                                    <th>Numero del Capitulo</th>
                                    <th>Nombre capitulo</th>
                                    <th>Acciones</th>
                                    </tr>
                            </thead>
                            <tbody>
                                @foreach ($capitulos as $capitulos)
                                <tr>
                                    <td>{{$capitulos->CapituloId}}</td>
                                    <td>{{$capitulos->NombreCapitulo}}</td>
                                    <td>
                                        <a href="{{route('EditarCap', $capitulos->CapituloId) }}" class="btn btn-primary">
                                            Modificar
                                        </a>
                                        <form action="{{ route('borrar',$capitulos->CapituloId)}}" method="POST">
                                            @csrf @method('DELETE')
                                            <button type="submit" onclick="return confirm('¿Estas seguro de borrar esta capitulo?');" class="btn btn-danger mt-2" >
                                                Eliminar 
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>  
                        </table>
                        
                        </h2>
                    </h2>
                </div>

                        
                               

        
     

     

    </div>


    </div>

</body>

</html>