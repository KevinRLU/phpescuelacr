<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/AgendarCita.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Editar cita</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-7 mt-5">
                <!--Mensaje flash-->
                @if(session('CitaModificada'))
                <div class="alert alert-success">
                    {{ session('CitaModificada') }}
                </div>
                @endif

                <!--Validación de errores-->
                @if($errors->any())
                <div class="alert alert-danger">
                     <ul>
                         @foreach($errors->all() as $error)
                         <li> {{ $error }}</li>
                         @endforeach
                     </ul>
                </div>
                @endif

                <div class="card">
                    <form action="{{ route('edit', $citas->CitaId) }}" method="POST">
                    @csrf @method('PATCH')
                        <div class="card-header text-center">Mofidicar Clase</div>
                        <div class="card-body">
                            <div class="row form-group">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Curso</label>
                                    <select class="form-control" value="{{$citas ->Modalidad }}" name="Modalidad" id="exampleFormControlSelect1">
                                      <option>Teoríco</option>
                                    </select>
                                  </div>
                                <div class="form-group">
                                    <label for="inputAddress">Fecha</label>
                                    <input type="date" value="{{$citas ->Fecha }}" name="Fecha">
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress2">Hora</label>
                                    <input type="time" value="{{$citas ->Hora }}" name="Hora">
                                </div>
                                    <div class="form-group">
                                        <label for="inputState">Cupos de estudiantes</label>
                                        <select id="inputState" name="CantidadCupos" class="form-control">
                                            <option value="{{$citas ->CantidadCupos }}" selected></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                        </select>
                                    </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Modificar</button>
                                </div>
                                

                            </div>

                        </div>   
                    </form>
                </div> 
            </div>
        </div>
        <a class="btn btn-light btn-xs mt-5" href="{{url('/CitasAdmin')}}">&laquo Volver></a>
    </div>

</body>

</html>