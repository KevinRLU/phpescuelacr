<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/public/css/app.css">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title>Examen Capítulo 1</title>
</head>
<body>
    <div class="container-fluid"> 

        <div class="row justify-content-center">
            <div class="card"> 
                <form action="{{ route('guardarresultados') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                  
                <div class="card-header text-center">
                    Examen Cápitulo 1<br>
                    Selecioné una unica respuesta por pregunta
               
                </div>
                <div class="card-body"> 
                    @php $preguntaactual = '' @endphp
                    @php Session::get('opcion');@endphp
                    
                    @foreach ($examenp as $pregunta)
                     
                        @if ($pregunta->Pregunta != $preguntaactual)
                    <fieldset>     
                                <legend>{{$pregunta->Pregunta}}</legend>
                                
                                     @if ($pregunta->img != null)
                                        <div class="conteiner-fluid" style="margin-left: 200px">
                                            <!-- SOLO IMAGENES PNG-->
                                             <img src="data:image/png;base64,{{ chunk_split(base64_encode($pregunta->img)) }}" height="150" width="150">
                                        </div>
                                    @endif
                                    @php $preguntaactual = $pregunta->Pregunta @endphp
                        @endif
                        
                                            
                    <div class="form-group">
                        <div class="form-check">
                            <!-- 
                            <input name="RespuestaId[]" type="hidden"  value="{{$pregunta->RespuestaId}}">
                            <input name="PreguntaId[]"  type="hidden"  value="{{$pregunta->PreguntaId}}">
                            <input name="Pregunta[]"    type="hidden"  value="{{$pregunta->Pregunta}}">
                            <input name="Respuesta[]"   type="hidden"  value="{{$pregunta->Respuesta}}">-->
                            <input class="form-check-input" type="checkbox" name="RespuestaId[]" value="{{$pregunta->RespuestaId}}">
                            <label class="form-check-label"  for="RespuestaId[]">

                            {{$pregunta->Respuesta}}
                            
                            </label>
                        </div>
                    </div> 
                    </fieldset>

                    @endforeach
                      <div class="form-group">
                        <button type="submit" class="btn btn-success btn-sm">Finalizar Examen</button>
                      </div>
                    </div>
                </form>
            </div>
       </div>
    </div>
</body>
</html>




