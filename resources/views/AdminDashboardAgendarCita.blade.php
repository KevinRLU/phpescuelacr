<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <title>Escuela de Manejo CR</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="css/AdminDashboard.css">
    <link rel="stylesheet" href="css/AgendarCita.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <input type="checkbox" id="sidebar-toggle">
    <div class="sidebar">
        <div class="sidebar-header">
            <h3 class="brand">
                <span>Administrador</span>
            </h3>
            <label for="sidebar-toggle" class="ti-menu-alt"></label>
        </div>

        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="">
                        <span class="ti-home"></span>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-user"></span>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-agenda"></span>
                        <span>Tareas</span>
                    </a>
                </li>
                <li>
                    <a href="/AgendarCitasAdmin">
                        <span class="ti-calendar"></span>
                        <span>Programar clases</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-settings"></span>
                        <span>Bitacora</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 mt-5">
                <!--Mensaje flash-->
                @if(session('Citaguardada'))
                <div class="alert alert-success">
                    {{ session('Citaguardada') }}
                </div>
                @endif

                <!--Validación de errores-->
                @if($errors->any())
                <div class="alert alert-danger">
                     <ul>
                         @foreach($errors->all() as $error)
                         <li> {{ $error }}</li>
                         @endforeach
                     </ul>
                </div>
                @endif

                <div class="card">
                    <form action="{{ route('save') }}" method="POST">
                    @csrf
                        <div class="card-header text-center">Agregar Clase</div>
                        <div class="card-body">
                            <div class="row form-group">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Curso</label>
                                    <select class="form-control" name="Modalidad" id="exampleFormControlSelect1">
                                      <option>Teoríco</option>
                                    </select>
                                  </div>
                                <div class="form-group">
                                    <label for="inputAddress">Fecha</label>
                                    <input type="date" name="Fecha">
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress2">Hora</label>
                                    <input type="time" name="Hora">
                                </div>
                                    <div class="form-group">
                                        <label for="inputState">Cupos de estudiantes</label>
                                        <select id="inputState" name="CantidadCupos" class="form-control">
                                            <option selected></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                        </select>
                                    </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                                

                            </div>

                        </div>   
                    </form>
                </div> 
            </div>
        </div>
        <a class="btn btn-light btn-xs mt-5" href="{{url('/CitasAdmin')}}">&laquo Volver></a>
    </div>


       
        

    

</body>

</html>