<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <section>
        <meta charset="utf-8">
        <title>Escuela de Manejo CR</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
    
        <!-- Favicons -->
        <link href="{{asset('/img/favicon.png') }}" rel="icon">
  <link href="{{asset('/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
          rel="stylesheet">
    
        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    
        <!-- Main Stylesheet File -->
        <link href="/css/style.css" rel="stylesheet">
    

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
     <link rel="stylesheet" href="{{asset('/css/informacion.css') }}">
    <title>Información Escuela de Manejo CR</title>
  </head>
  <body>
    <header id="header" class="fixed-top">
        <div class="container">
    
          <div class="logo float-left">
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
            <a href="/" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
          </div>
    
          <nav class="main-nav float-right d-none d-lg-block">
            <ul>
              <li><a href="/">Inicio</a></li>
              <li class="drop-down"><a href="/capitulos">Lector del manual</a>
                <ul>
                  <li><a href="#">Capítulo 1</a></li>
                  <li><a href="#">Capítulo 2</a></li>
                  <li><a href="#">Capítulo 3</a></li>
                  <li><a href="#">Capítulo 4</a></li>
                  <li><a href="#">Capítulo 5</a></li>
                  <li><a href="#">Capítulo 6</a></li>
                  <li><a href="#">Capítulo 7</a></li>
                  <li><a href="#">Capítulo 8</a></li>
                  <li><a href="#">Capítulo 9</a></li>
                  <li><a href="#">Capítulo 10</a></li>
                </ul>
              </li>
              <li><a href="/galeria">Galería</a></li>
              <li><a href="/informacion">Información</a></li>
    
              <li><a href="/contacto">Contacto</a></li>
              <li><a href="/perfil">Perfil</a></li>
              <li><a href="/login">Iniciar Sesión</a></li>
              <li><a href="/register">Registrarse</a></li>
            </ul>
          </nav><!-- .main-nav -->
    
        </div>
      </header>
      <body >
        <!-- partial:index.partial.html -->
        <body>
          <div class="wrap" >
            <ul class="tabs">
              <li><a href="#tab1"><span class="fa fa-id-card"></span><span class="tab-text">Requisitos</span></a></li>
              <li><a href="#tab2"><span class="fa fa-car"></span><span class="tab-text">Licencias</span></a></li>
              <li><a href="#tab3"><span class="fa fa-info"></span><span class="tab-text">Información</span></a></li>
              <li><a href="#tab4"><span class="fa fa-legal"></span><span class="tab-text">Importante</span></a></li>
            </ul>
        
            <div class="secciones">
              <article id="tab1">
                <h1>Requisitos para licencias</h1>
                <p>Cada tipo de licencia tiene requisitos que debe cumplir para obtenerla. Para automóvil (licencia B-1)
                   los requisitos son ser mayor 18 años y cumplir con los exámenes teórico y práctico. Cuidado con las licencias B2, B3, B4 y las C, que tienen requisitos especiales, revíselos con cuidado! En la sección de tipos de licencia de conducir, podrá encontrar los requisitos para licencias, en cada caso.</p>
                <p>En todos los casos, se ocupa el dictamen médico digital, que esté vigente hasta el momento en que tenga en la mano la licencia. No olvide revisar que el dictamen y la cédula tengan la vigencia necesaria! No hay nada peor que empezar el trámite de la licencia de conducir, y a medio camino encontrar que se venció un documento y hay que volver a obtenerlo.</p>
              </article>
              <article id="tab2">
                <h1>Tipos de Licencia</h1>
                <p>Ofrecemos Clases para poder obtener las licencias de estos diferentes tipos</p><br>
                <h1>Licencias TIPO A (A1,A2,A3)</h1><br>
                <p>A1: Cilindrada De 0 a 125cc, lo cual cubre bicimotos, motocicletas</p><br>
                <p>A2: Cubre Cilindrada De 126 cc a 500 cc.(Bicimotos y Motocicletas) de 256cc hasta 500cc sean cuadraciclos o triciclos</p><br>
                <p>A3: Cilindrada De 501 cc en adelante. </p>



                <h1>Licencias TIPO B (B1,B2)</h1>
                <p>B1: Cubre Vehículos de máximo 4000 kg. Ademas, la licencia B1 te autoriza a conducir bicimotos o motocicletas cuyo cilindraje no supere los 125 centímetros cúbicos.</p><br>
                <p>B2: Con esta licencia se puede conducir vehículos o camiones de hasta 8 toneladas de peso bruto o peso máximo autorizado (lo que sea mayor). Los remolques están permitidos siempre y cuando el peso del vehículo más el remolque no exceda las 8 toneladas</p><br>


              </article>
              <article id="tab3">
                <h1>Servicios</h1>
                <p>✓ Cursos Teóricos.</p>
                 <p>✓ Pruebas Prácticas.</p>
                  <p>✓ Citas Teóricas y Prácticas.</p>
                  <p>✓ Alquiler de Vehículos para Prueba de COSEVI.</p>
                 <p>✓ A1,A2, B1, B2.</p>
                  <p>✓ Clases de Manejo (Todos los niveles)</p>
                  <p>✓ Asesorias de parte del equipo de la escuela</p>
                  <p>✓ Dictamen medico</p>
              </article>
              <article id="tab4">
                <h1>Importante</h1>
                <p>La matrícula de pruebas teóricas y prácticas de COSEVI se puede hacer vía Internet, usando el sitio de la Dirección General de Educación Vial, o vía telefónica en el call center de esta misma oficina.

                  Y por supuesto, recuerde que no hay atajos ni «favores» que valgan a la hora de sacar la licencia de conducir. No pague «compras» de licencias, ni le crea a la gente que ofrece licencias «sin examen» por Internet. Probablemente terminará con una licencia falsa, que no está correctamente inscrita en COSEVI, y estará en problemas si lo detienen en carretera, o cuando le toque renovar la licencia.</p><br>

              </article>
            </div>
          </div>
        </body>
        <!-- partial -->
          <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script><script  src="/js/informacion.js"></script>
        
        </body>
     
    
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->

  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Escuela de Manejo CR</h3>
            <p>La escuela de manejo CR esta abierta para todos aquellas personas que quieran aprender a manejar.</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Enlaces</h4>
            <ul>
              <li><a href="/">Inicio</a></li>
              <li><a href="/informacion">informacion</a></li>
              <li><a href="/galeria">Galería</a></li>
              <li><a href="/contacto">Contactenos</a></li>
            </ul>
          </div>
  
          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contactanos</h4>
            <p>
              126,Santa Lucia,
              Provincia de Heredia,
              Heredia, 40205, 75 Oeste de Mc Donald's <br>
              <strong>Telefóno:</strong> +506 6026-2525<br>
              <strong>Correo electrónico:</strong> escuelademanejocr1@gmail.com<br>
            </p>
  
            <div class="social-links">
  
              <a href="https://www.facebook.com/escuelademanejoericka/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/escuelademanejocr" class="instagram"><i class="fa fa-instagram"></i></a>
              

            </div>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; <strong>Escuela Manejo CR</strong>.
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->

      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
</body>
</html>
