<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <title>Escuela de Manejo CR</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="css/AdminDashboard.css">
    <link rel="stylesheet" href="css/AgendarCita.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <input type="checkbox" id="sidebar-toggle">
    <div class="sidebar">
        <div class="sidebar-header">
            <h3 class="brand">
                <span>Admin</span>
            </h3>
            <label for="sidebar-toggle" class="ti-menu-alt"></label>
        </div>

        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="">
                        <span class="ti-home"></span>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-user"></span>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-agenda"></span>
                        <span>Tareas</span>
                    </a>
                </li>
                <li>
                    <a href="AdminDashboardCitas.html">
                        <span class="ti-calendar"></span>
                        <span>Programar clases</span>
                    </a>
                </li>
                <li>
                    <a href="/AdminCapitulos">
                        <span class="ti-book"></span>
                        <span>Capitulos</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 mt-5">
                <!--Mensaje flash-->
                @if(session('Capguardado'))
                <div class="alert alert-success">
                    {{ session('Capguardado') }}
                </div>
                @endif

                <!--Validación de errores-->
                @if($errors->any())
                <div class="alert alert-danger">
                     <ul>
                         @foreach($errors->all() as $error)
                         <li> {{ $error }}</li>
                         @endforeach
                     </ul>
                </div>
                @endif

                <div class="card">
                    <form action="{{ route('savee') }}" method="POST">
                    @csrf
                        <div class="card-header text-center">Agregar Capitulo</div>
                        <div class="card-body">
                            <div class="row form-group">
                                <div class="form-group">
                                    <label for="name">Numero del Capitulo</label>
                                    <input type="text" name="CapituloId" />
                                    </div>
                                <div class="form-group">
                                    <label for="name">Nombre Capitulo</label>
                                    <input type="text" name="NombreCapitulo" />
                                    </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </div>   
                        
                    </form>
                </div> 
                <a class="btn btn-light btn-xs mt-5" href="{{url('/AdminCapitulos')}}">< Volver></a>
            </div>
        </div>
        
        
    </div>
    
</body>


       
        

    

</html>