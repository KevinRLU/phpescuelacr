<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <title>Escuela de Manejo CR</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
    
        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
          rel="stylesheet">
    
        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    
        <!-- Main Stylesheet File -->
        <link href="css/style.css" rel="stylesheet">
    

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
     <link rel="stylesheet" href="css/galeria.css">
    <title>Galeria Escuela de Manejo CR</title>
  </head>
  <body>
    <section>
    <header id="header" class="fixed-top">
        <div class="container">
    
          <div class="logo float-left">
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
            <a href="/" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
          </div>
    
          <nav class="main-nav float-right d-none d-lg-block">
            <ul>
              <li class="active"><a href="/">Inicio</a></li>
              <li class="drop-down"><a href="/capitulos">Lector del manual</a>
                <ul>
                  <li><a href="#">Capítulo 1</a></li>
                  <li><a href="#">Capítulo 2</a></li>
                  <li><a href="#">Capítulo 3</a></li>
                  <li><a href="#">Capítulo 4</a></li>
                  <li><a href="#">Capítulo 5</a></li>
                  <li><a href="#">Capítulo 6</a></li>
                  <li><a href="#">Capítulo 7</a></li>
                  <li><a href="#">Capítulo 8</a></li>
                  <li><a href="#">Capítulo 9</a></li>
                  <li><a href="#">Capítulo 10</a></li>
                </ul>
              </li>
              <li><a href="/galeria">Galería</a></li>
              <li><a href="/informacion">Información</a></li>
    
              <li><a href="/contacto">Contacto</a></li>
              <li><a href="/login">Iniciar Sesión</a></li>
              <li><a href="/register">Registrarse</a></li>
            </ul>
          </nav><!-- .main-nav -->
    
        </div>
      </header>
    <section id="galeria" class="container">
        <div class="text-center pt-5" >
            <h1 style="margin: 50px 0 20px 0;">Galeria</h1>

        
		
					<path
						d="M0.00,49.98 C86.05,146.53 273.98,-84.38 500.00,49.98 L500.00,150.00 L0.00,150.00 Z"
						style="stroke: none; fill: #fff;"
					></path>
				</svg>
			</div>
		</div>

		<div class="galeria">
			<div class="foto">
				<div class="contenedor-modal">
					<img src="img/imagen1.jpg" alt="" />
					<div class="overlay">
						<h2>Demostracion Escuela</h2>
					</div>
				</div>
			</div>

			<div class="foto">
				<div class="contenedor-modal">
					<img src="img/imagen2.jpg" alt="" />
					<div class="overlay">
						<h2>Clases Virtuales</h2>
					</div>
				</div>
			</div>
			<div class="foto">
				<div class="contenedor-modal">
					<img src="img/imagen3.jpg" alt="" />
					<div class="overlay">
						<h2>Plantel Heredia</h2>
					</div>
				</div>
			</div>
			<div class="foto">
				<div class="contenedor-modal">
					<img src="img/imagen4.jpg" alt="" />
					<div class="overlay">
						<h2>Alumno Licencia</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="galeria">
			<div class="foto">
				<div class="contenedor-modal">
					<img src="img/imagen1.jpg" alt="" />
					<div class="overlay">
						<h2>Demostracion Escuela</h2>
					</div>
				</div>
			</div>
    </section>
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Escuela de Manejo CR</h3>
            <p>La escuela de manejo CR esta abierta para todos aquellas personas que quieran aprender a manejar.</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Enlaces</h4>
            <ul>
              <li><a href="/index">Inicio</a></li>
              <li><a href="/informacion">Acerca de</a></li>
              <li><a href="/informacion">Información</a></li>
            </ul>
          </div>
  
          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contactanos</h4>
            <p>
              126,Santa Lucia,
              Provincia de Heredia,
              Heredia, 40205, 75 Oeste de Mc Donald's <br>
              <strong>Telefóno:</strong> +506 6026-2525<br>
              <strong>Correo electrónico:</strong> escuelademanejocr1@gmail.com<br>
            </p>
  
            <div class="social-links">
  
              <a href="https://www.facebook.com/escuelademanejoericka/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/escuelademanejocr" class="instagram"><i class="fa fa-instagram"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="container">
      <div class="copyright">
        &copy; <strong>Escuela Manejo CR</strong>.
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->

      </div>
    </div>
  </footer><!-- #footer -->
</body>
<script src="js/galeria.js"></script>
<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
</html>
