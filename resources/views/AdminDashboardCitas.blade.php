<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <title>Escuela de Manejo CR</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="css/AdminDashboard.css">
    <link rel="stylesheet" href="css/IndexCitas.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <input type="checkbox" id="sidebar-toggle">
    <div class="sidebar">
        <div class="sidebar-header">
            <h3 class="brand">
                <span>Administrador</span>
            </h3>
            <label for="sidebar-toggle" class="ti-menu-alt"></label>
        </div>

        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="">
                        <span class="ti-home"></span>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-user"></span>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-agenda"></span>
                        <span>Tareas</span>
                    </a>
                </li>
                <li>
                    <a href="AdminDashboardCitas.html">
                        <span class="ti-calendar"></span>
                        <span>Programar clases</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <span class="ti-settings"></span>
                        <span>Bitacora</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>


    <div class="main-content">
        
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-10">
                <h2 class="text-center mb-5">Clases</h2>
                <a class="btn btn-success mb-2" href="{{url('/AgendarCitaAdmin')}}">Agregar Clase</a>
                    <!--Mensaje Flash-->
                    @if (session('CitaEliminada'))
                    <div class="alert alert-success">
                    {{ session('CitaEliminada') }}
                    </div>
                    @endif

                <table class="table table-bordered table-striped text-center">
                    <thead> 
                        <tr>
                            <th>Modalidad</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Cantidad de Cupos</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($citas as $cita)
                        <tr>
                            <td>{{ $cita->Modalidad }}</td>
                            <td>{{ $cita->Fecha }}</td>
                            <td>{{ $cita->Hora }}</td>
                            <td>{{ $cita->CantidadCupos }}</td>
                            <td>
                                <a href="{{route('EditarCita', $cita->CitaId) }}" class="btn btn-primary">
                                    Modificar
                                </a>
                                <form action="{{ route('delete',$cita->CitaId)}}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" onclick="return confirm('¿Estas seguro de borrar esta clase?');" class="btn btn-danger mt-2" >
                                        Eliminar 
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$citas->links()}}

                </div>
            </div>

        </div>

     

    </div>


    </div>

</body>

</html>