<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/AgendarCita.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Editar cita</title>
    </head>

<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-7 mt-5">
                <!--Mensaje flash-->
                @if(session('CapModificados'))
                <div class="alert alert-success">
                    {{ session('CapModificados') }}
                </div>
                @endif

                <!--Validación de errores-->
                @if($errors->any())
                <div class="alert alert-danger">
                     <ul>
                         @foreach($errors->all() as $error)
                         <li> {{ $error }}</li>
                         @endforeach
                     </ul>
                </div>
                @endif

                <div class="card">
                    <form action="{{ route('edit', $capitulos->id) }}" method="POST">
                    @csrf @method('PATCH')
                        <div class="card-header text-center">Mofidicar Capitulos</div>
                        <div class="card-body">
                            <div class="row form-group">   
                                   <div class="form-group">
                                   <label for="name">Nombre Capitulo</label>
                                   <input type="text" value="{{$capitulos ->NombreCapitulo}}" name="NombreCapitulo" />
                                   </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Modificar</button>
                                </div>
                            </div>
                        </div>   
                    </form>
                </div> 
            </div>
        </div>
        <a class="btn btn-light btn-xs mt-5" href="{{url('/AdminCapitulos')}}">< Volver></a>
    </div>
    </div>


    </div>

</body>

</html>