<!DOCTYPE html>
<html lang="en">



    <div class="main-content">
        
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-10">
                <h2 class="text-center mb-5">Clases</h2>
                <a class="btn btn-success mb-2" href="{{url('/AgendarCitaAdmin')}}">Agregar Clase</a>
                    <!--Mensaje Flash-->
                    @if (session('CitaEliminada'))
                    <div class="alert alert-success">
                    {{ session('CitaEliminada') }}
                    </div>
                    @endif

                <table class="table table-bordered table-striped text-center">
                    <thead> 
                        <tr>
                            <th>Modalidad</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Cantidad de Cupos</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($citas as $cita)
                        <tr>
                            <td>{{ $cita->Modalidad }}</td>
                            <td>{{ $cita->Fecha }}</td>
                            <td>{{ $cita->Hora }}</td>
                            <td>{{ $cita->CantidadCupos }}</td>
                            <td>
                                <a href="{{route('EditarCita', $cita->CitaId) }}" class="btn btn-primary">
                                    Modificar
                                </a>
                                <form action="{{ route('delete',$cita->CitaId)}}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" onclick="return confirm('¿Estas seguro de borrar esta clase?');" class="btn btn-danger mt-2" >
                                        Eliminar 
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$citas->links()}}

                </div>
            </div>

        </div>

     

    </div>


    </div>

</body>

</html>