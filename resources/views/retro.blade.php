<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/public/css/app.css">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title>Resultados</title>
</head>
<body>
    <!--Validación de errores-->
    @if($errors->any())
    <div class="alert alert-danger">
         <ul>
             @foreach($errors->all() as $error)
             <li> {{ $error }}</li>
             @endforeach
         </ul>
    </div>
    @endif
    <div class="container-fluid"> 
                <form action="{{ route('save') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                  
                <div class="card-header text-center">
                    Resultados<br>
                </div>
                <div class="card-body"> 
                    @php $preguntaactual = '' @endphp
                    @foreach ($resultados as $resultado)              
                    <fieldset>
                        @if ($resultado->Pregunta != $preguntaactual)
                        <legend>{{$resultado->Pregunta}}</legend>

                        <p class="font-weight-normal">Respuesta seleccionada: {{$resultado->Respuesta}}</p>
                        @if ($resultado->Opcion == 0)
                        <p class="text-danger">Incorrecta</p>
                        <div class="p-3 mb-2 bg-info text-white">Respuesta completa: {{$resultado->Retro}}</div>
                        @else
                        <p class="text-success">Correcta</p>
                        @endif
                        
                        @php $preguntaactual = $resultado->Pregunta @endphp
                     
                     
                    </fieldset>
                    
                    @endif
                    @endforeach
                   
                    </div>
                </form>
          

    </div>
    <a class="btn btn-light btn-xs mt-5" href="{{url('/examen')}}">&laquo Volver</a>
</body>
</html>




