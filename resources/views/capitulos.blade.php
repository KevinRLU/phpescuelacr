<!DOCTYPE html>
<head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <title>Escuela de Manejo CR</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
    
        <!-- Favicons -->
        <link href="{{asset('/img/favicon.png') }}" rel="icon">
  		<link href="{{asset('/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
          rel="stylesheet">
    
        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	
    
        <!-- Main Stylesheet File -->
        <link href="/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/demo.css" />
    
	    <!-- Main Stylesheet File -->
		<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	    <link href= "css/magnific-popup.css" rel="stylesheet">
	    <link href="css/templatemo_style.css" rel="stylesheet" type="text/css">
    

		<!-- Bootstrap CSS -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
     
		<title>Capitulos Escuela de Manejo CR</title>
		<header id="header" class="fixed-top">
			<div class="container">
		
			<div class="logo float-left">
				<!-- Uncomment below if you prefer to use an image logo -->
				<!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
				<a href="/index" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
			</div>
		
			<nav class="main-nav float-right d-none d-lg-block">
				<ul>
				<li class="active"><a href="/">Inicio</a></li>
				<li><a href="/capitulos">Lector del manual</a>
				</li>
				<li><a href="/galeria">Galería</a></li>
				<li><a href="/informacion">Información</a></li>
		
				<li><a href="/contacto">Contacto</a></li>
				<li><a href="/perfil">Perfil</a></li>
				<li><a href="/login">Iniciar Sesión</a></li>
				<li><a href="/register">Registrarse</a></li>
				</ul>
			</nav><!-- .main-nav -->
		
			</div>
		</header>
</head>
<body>
	<div class="contenido-container">
		<header>
			<h1 class="center-text">Capitulo 1</h1>
		
		</header>
		<div id="portfolio-content" class="center-text">
			<div class="portfolio-page" id="page-1">
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo1.png">
						<img src="img/capitulo1.png" alt="image 1">
						<div class="detail">
							<h3>Antecedentes Historicos</h3>
							<p>Capitulo 1</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo1-2.png">
						<img src="img/capitulo1-2.png" alt="image 2">
						<div class="detail">
							<h3>Trilogia Vial</h3>
							<p>Capitulo 1</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo1-3.png">
						<img src="img/capitulo1-3.png" alt="image 3">
						<div class="detail">
							<h3>Que es seguidad vial</h3>
							<p>Capitulo 1</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo2.jpg">
						<img src="img/capitulo2.jpg" alt="image 4">
						<div class="detail">
							<h3>Legislacion de transito</h3>
							<p>Capitulo 2.</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo2-1.png">
						<img src="img/capitulo2-1.png" alt="image 5">
						<div class="detail">
							<h3>Sanciones</h3>
							<p>Capitulo 2.</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
				<div class="portfolio-group">
					<a class="portfolio-item" href="img/capitulo2-3.png">
						<img src="img/capitulo2-3.png" alt="image 6">
						<div class="detail">
							<h3>Renovaciones</h3>
							<p>Capitulo 2</p>
							<span class="btn">Ver</span>
						</div>
					</a>
				</div>
			
			
			</div> <!-- page 3 -->
			<div class="pagination">
				<ul class="nav">
					<li class="active">1</li>
					<li>2</li>
				</ul>
			</div>
		</div>
	</div> <!-- /.content-container -->
		 
	<footer id="footer">
		<div class="footer-top">
		<div class="container">
			<div class="row">
	
			<div class="col-lg-4 col-md-6 footer-info">
				<h3>Escuela de Manejo CR</h3>
				<p>La escuela de manejo CR esta abierta para todos aquellas personas que quieran aprender a manejar.</p>
			</div>
	
			<div class="col-lg-2 col-md-6 footer-links">
				<h4>Enlaces</h4>
				<ul>
				<li><a href="/">Inicio</a></li>
				<li><a href="/informacion">Informacion</a></li>
				<li><a href="/galeria">Galería</a></li>
				<li><a href="/contacto">Contactenos</a></li>
				</ul>
			</div>
	
			<div class="col-lg-3 col-md-6 footer-contact">
				<h4>Contactanos</h4>
				<p>
				126,Santa Lucia,
				Provincia de Heredia,
				Heredia, 40205, 75 Oeste de Mc Donald's <br>
				<strong>Telefóno:</strong> +506 6026-2525<br>
				<strong>Correo electrónico:</strong> escuelademanejocr1@gmail.com<br>
				</p>
	
				<div class="social-links">
	
				<a href="https://www.facebook.com/escuelademanejoericka/" class="facebook"><i class="fa fa-facebook"></i></a>
				<a href="https://www.instagram.com/escuelademanejocr" class="instagram"><i class="fa fa-instagram"></i></a>
				
	
				</div>
	
			</div>
	
			</div>
		</div>
		</div>
	
		<div class="container">
		<div class="copyright">
			&copy; <strong>Escuela Manejo CR</strong>.
		</div>
		<div class="credits">
			<!--
			All the links in the footer should remain intact.
			You can delete the links only if you purchased the pro version.
			Licensing information: https://bootstrapmade.com/license/
			Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
			-->
	
		</div>
		</div>
	</footer><!-- #footer -->
</body>



		 <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
		 <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
		 <script type="text/javascript" src="js/modernizr.2.5.3.min.js"></script>
		 <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
		 <script type="text/javascript" src="js/templatemo_script.js"></script>
		 <script type="text/javascript">
			 $(function () {
				 $('.pagination li').click(changePage);
				 $('.portfolio-item').magnificPopup({
					 type: 'image',
					 gallery: {
						 enabled: true
					 }
				 });
			 });
		 </script>
		 <!-- partial -->
		 <script  src="/js/informacion.js"></script>

		 
			  
		 <!-- Optional JavaScript; choose one of the two! -->
	 
		 <!-- Option 1: Bootstrap Bundle with Popper -->
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	 
		 <!-- Option 2: Separate Popper and Bootstrap JS -->
		 <!--
		 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
		 -->
</html>