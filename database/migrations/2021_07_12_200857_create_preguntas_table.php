<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('preguntas', function (Blueprint $table) {
            $table->bigIncrements('PreguntaId');
            $table->string('Pregunta');
            $table->string('Retro',600);
            $table->binary('img');
            $table->timestamps();
           
        });

        Schema::table('preguntas', function($table) {
            
            $table->Integer('CapituloId');
            $table->foreign('CapituloId')->references('CapituloId')->on('capitulos')->onDelete('cascade');
        
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
