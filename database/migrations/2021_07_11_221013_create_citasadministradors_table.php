<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasadministradorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citasadministradors', function (Blueprint $table) {
            $table->bigIncrements('CitaId');
            $table->string('Modalidad');
            $table->date('Fecha');
            $table->time('Hora');
            $table->integer('CantidadCupos');
            $table->integer('Estado')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citaadministradors');
    }
}
