<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('respuestas', function (Blueprint $table) {
            $table->bigIncrements('RespuestaId');  
            $table->string('Respuesta');
            $table->decimal('Opcion',10,0);
            $table->timestamps();      
        });

        Schema::table('respuestas', function($table) {
            $table->unsignedBigInteger('PreguntaId');
            $table->foreign('PreguntaId')->references('PreguntaId')->on('preguntas')->onDelete('cascade')->onUpdate('cascade');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas');
    }
}
