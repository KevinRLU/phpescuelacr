<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\citasadministradors;

class CitasAdministradorController extends Controller
{
// listado de citas

    public function list(){
        $data['citas'] = citasadministradors::paginate(20);

        return view('AdminDashboardCitas', $data);
    }


    // Devuelve la vista de Agendarcita
    public function citasadminform(){
        return view('AdminDashboardAgendarCita.blade.php');
    }

    //Se guardan las citas
    public function save(Request $request){

        $validator = $this->validate($request,[
            'Modalidad'=> 'required|string|max:255',
            'Fecha'    => 'required|unique:citasadministradors|',
            'Hora'     => 'required',
            'CantidadCupos'=> 'required'
        ]);

        $userdata = request()->except('_token');
        citasadministradors::insert($userdata);

        return back()->with('Citaguardada','Cita guardada');
    }

    //Eliminar citas
    public function delete($CitaId)
    {
        citasadministradors::destroy($CitaId);
        return back()->with('CitaEliminada','Cita eliminada');
    }

    //Formulario para editar citas
    public function EditarCita($CitaId){
        $citas = citasadministradors::findOrFail($CitaId);

        return view('EditarCita', compact('citas'));
    }
    //Se guardan las citas
    public function edit(Request $request, $CitaId){
        $citasdata = request()->except((['_token','_method']));
        citasadministradors::where('CitaId','=', $CitaId)->update($citasdata);

        return back()->with('CitaModificada','Cita Modificada');
    }

    

}

