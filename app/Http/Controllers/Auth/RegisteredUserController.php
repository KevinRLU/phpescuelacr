<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'Cedula' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'Apellido1' => ['required', 'string', 'max:255'],
            'Apellido2' => ['required', 'string', 'max:255'],
            'Telefono' => ['required', 'string', 'max:8'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        Auth::login($user = User::create([
            'Cedula' => $request->Cedula,
            'name' => $request->name,
            'email' => $request->email,
            'Apellido1' => $request->Apellido1,
            'Apellido2' => $request->Apellido2,
            'Telefono' => $request->Telefono,
            'password' => Hash::make($request->password),
        ]));
        $user->attachRole($request->role_id); 
        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
