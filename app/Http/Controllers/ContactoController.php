<?php



namespace App\Http\Controllers;


use App\Mail\ContactoMail;
use Illuminate\Http\Request;


class ContactoController extends Controller
{
    public function contacto()
    {
        return view('contacto');
    }
}
