<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pregunta;

class PreguntasController extends Controller
{
    public function list(){
        $data['preguntas'] = Preguntas::paginate(20);
    
        return view('AdminCapitulos', $data);
    }
    
    
    // Devuelve la vista de las preguntas ya almacenadas
    public function preguntasadminform(){
        return view('AdminPreguntas.blade.php');
    }
    
    //Se guardan las preguntas
    public function saveee(Request $request){
    
        $validator = $this->validate($request,[
            'NombreCap'=> 'required|string|max:255',
            'Retro'=> 'required',
            'img'
            
        ]);
    
        $userdata = request()->except('_token');
        Preguntas::insert($userdata);
    
        return back()->with('Preguardada','Pregunta guardada');
    }
    
    //Eliminar preguntas
    public function delete($id)
    {
        Preguntas::destroy($id);
        return back()->with('PregEliminado','Pregunta eliminada');
    }
    
    //Formulario para editar las preguntas
    public function EditarPreg($id){
        $capitulos = Preguntas::findOrFail($id);
    
        return view('EditarPreg', compact('preguntas'));
    }
    //Se guardan las preguntas
    public function edit(Request $request, $id){
        $pregdata = request()->except((['_token','_method']));
        Preguntas::where('id','=', $id)->update($pregdata);
    
        return back()->with('PregModificada','Pregunta Modificado');
    }
    
}
