<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Capitulo;

class CapitulosController extends Controller
{
    //Metodo para el listado 
    public function list(){
    $data['capitulos'] = Capitulo::paginate(11);

    return view('AdminCapitulos', $data);
}


// Devuelve la vista de Capitulos
public function capitulosadminform(){
    return view('AdminCapitulos.blade.php');
}

//Se guardan los Capitulos
public function savee(Request $request){

    //Se guarda los parametros en la tabla
    $validator = $this->validate($request,[
        'CapituloId'=>'required|unique:capitulos|',
        'NombreCapitulo'=> 'required|string|max:255',
        
    ]);

    $userdata = request()->except('_token');
    Capitulo::insert($userdata);

    return back()->with('Capguardado','Capitulo guardado');
}

//Eliminar capitulos
public function borrar($CapituloId)
{
    Capitulo::destroy($CapituloId);
    return back()->with('CapEliminado','Capitulo eliminado');
}

//Formulario para editar capitulos
public function EditarCap($CapituloId){
    $capitulos = Capitulo::findOrFail($CapituloId);

    return view('EditarCap', compact('capitulos'));
}
//Se guardan los capitulos
public function edit(Request $request, $CapituloId){
    $capsdata = request()->except((['_token','_method']));
    Capitulo::where('CapituloId','=', $CapituloId)->update($capsdata);

    return back()->with('CapModificados','Capitulos Modificado');
}

  
}

