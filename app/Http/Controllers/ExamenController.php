<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Capitulo;
use App\Models\Pregunta;
use App\Models\Respuesta;
use App\Models\Resultado;
use App\Http\Controllers\Auth;
use Session;
use View;

class ExamenController extends Controller
{

    
    public function exam()
    {
        $examenp = DB::table('preguntas')
                    ->join('respuestas', function ($join){
                        $join->on('preguntas.PreguntaId','=','respuestas.PreguntaId')
                        ->where('preguntas.CapituloId','=',1);        
                    })->take(50)->get();


        return view('examen')->with('examenp',$examenp);
    }
//Método para guardar resultados y luego mostrarlos

    public function guardarresultados(Request $request){

        $validator = $this->validate($request,[
            'RespuestaId'=> 'required_without_all',
        ]);

        // La variable $RespuestaId recibe un array del input que esta en los checkbox de las vistas
        $RespuestaId = $request['RespuestaId'];
 
       
        // Se recorre todo el array en este foreach
        foreach ($RespuestaId as $resultdat) {
 
         //Primero se crea una nueva instacia del modelo de Resultado, luego se le pasa el valor del id y luego se guarda
         $resultado = new Resultado();

         //Se le asigna el id en el array en una posición n
         $resultado->RespuestaId = $resultdat;
         //Se almacena el valor dentro de la base de datos
         $resultado->save();
 
         }

     
        //La variable $resultados recibe los datos de las tablas respuestas y preguntas 
        $resultados = DB::table('resultados')
                    ->join('respuestas', 'resultados.RespuestaId', '=', 'respuestas.RespuestaId')
                    ->join('preguntas', 'respuestas.PreguntaId', '=', 'preguntas.PreguntaId')
                    ->select('preguntas.Pregunta','preguntas.Retro','respuestas.Respuesta','respuestas.Opcion')
                    ->where('preguntas.CapituloId','=',1)
                    ->take(50)
                    ->get();

        
 
         
     //Devolvemos una vista con la variable $resultados para poder usar
     //los datos de las tablas respuestas y preguntas
         return View::make('retro')->with('resultados',$resultados);
         
    }



    
}
