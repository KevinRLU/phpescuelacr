<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class citasadministradors extends Model
{
    use HasFactory;
    protected $primaryKey = 'CitaId';
}
