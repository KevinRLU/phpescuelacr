<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capitulo extends Model
{
    
    protected $table = 'capitulos';
    protected $primaryKey = 'CapituloId';
   
}
