<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    
    protected $table = 'preguntas';
    protected $primaryKey = 'PreguntaId';
    
    protected $fillable = ['PreguntaId','Pregunta','Retro','img'];
    
    
}
