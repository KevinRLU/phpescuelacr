<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CitasEstudiante extends Model
{
    use HasFactory;

    protected $table = 'citasestudiantes';
    protected $primaryKey = 'id';
  
    protected $fillable = ['id','IdEstudiante','IdCita'];

    public $timestamps=false;

}
